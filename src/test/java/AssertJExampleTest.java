import org.assertj.core.api.Assertions;
import org.assertj.core.data.Percentage;
import org.junit.jupiter.api.Test;

public class AssertJExampleTest {
    @Test
    void adunareNumereTest() {
        int result = 1 + 3;

        Assertions.assertThat(result)
                // .isEqualTo(4)
                .isNotEqualTo(5)
                .isLessThan(6)
                .isGreaterThan(2)
                .isBetween(0,10)
                .isCloseTo(5, Percentage.withPercentage(30));
    }

}
