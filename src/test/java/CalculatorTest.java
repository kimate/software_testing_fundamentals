import org.example.math.Calculator;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledIf;


public class CalculatorTest {
    @Test
    @Disabled
    @DisplayName("testare suma")
    void sumaTest() {
        Calculator calculator = new Calculator();
        int result = calculator.sum(2, 3);
        Assertions.assertEquals(5, result);
    }

    @BeforeEach
    void afisareMesajStart() {
        System.out.println("Incepe un test");
    }

    @AfterEach
    void afisareMesajEnd() {
        System.out.println("Incheie un test");
    }

    @Test
    @DisplayName("testare inmultire")
    void inmultireTest() {
       // se da - given
        Calculator calculator = new Calculator();

        //cand - when
        int result = calculator.inmultire(2, 3);

        //atunci - then
        Assertions.assertEquals(6, result);
    }

    @Test
    @DisplayName("testare scadere")
    void scadereTest() {
        // se da - given
        Calculator calculator = new Calculator();

        //cand - when
        int result = calculator.difference(6, 4);

        //atunci - then
        Assertions.assertEquals(2, result);

    }

    @Test
    @DisplayName("testare impartire")
    void impartireTest() {
        // se da - given
        Calculator calculator = new Calculator();
        //cand - when
        double result = calculator.impartire(8, 4);
        //atunci - then
        Assertions.assertNotEquals(3, result);
    }

    @BeforeAll
    static void afisareMesajBunVenit() {
        System.out.println("Bun venit la testarea calculatorului");
    }

    @AfterAll
    static void afisareMesajLaRevedere() {
        System.out.println("Testele sau finalizat, o zi buna");
    }

}
