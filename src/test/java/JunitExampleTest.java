import org.junit.jupiter.api.*;

public class JunitExampleTest {
    @Test
    void egalitateTextTest() {
        // given
        String text1 = "1";
        String text2 = "1";
        //when
        boolean result = text1.equals(text2);
        //then
        Assertions.assertNotEquals(0, text1.length());
        Assertions.assertEquals(true, result);
    }

    @Test
    void testNotNull() {
        String text = "Java remote RO69";
        Assertions.assertNotNull(text, "Testing using assertNotNull");
    }

    @Test
    void testTrue() {
        boolean value = true;
        Assertions.assertTrue(value);
    }

    @Test
    void testFalse() {
        boolean value = false;
        Assertions.assertFalse(value);

    }
    @AfterEach
    void afisareMesajDupaFiecareTest(){
        System.out.println("Java remote RO69.");
    }
    @BeforeAll
    static void afisareMesajInainteDeTeste() {
        System.out.println("buna ziua");
    }
    @AfterAll
    static void afisareMesajDupaExecutareTeste (){
        System.out.println("hello Java");

    }

}
