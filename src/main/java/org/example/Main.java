package org.example;

import org.example.math.Calculator;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        int result = calculator.sum(2, 3);
        System.out.println(result);
    }
}